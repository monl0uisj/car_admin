<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Color
 *
 * @ORM\Table(name="color")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ColorRepository")
 */
class Color
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="color")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="CarHasColor", mappedBy="color")
     */
    private $carhascolor;

    public function __construct() {
        $this->users = new ArrayCollection();
        $this->carhascolor = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Color
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Color
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add carhascolor
     *
     * @param \AppBundle\Entity\CarHasColor $carhascolor
     *
     * @return Color
     */
    public function addCarhascolor(\AppBundle\Entity\CarHasColor $carhascolor)
    {
        $this->carhascolor[] = $carhascolor;

        return $this;
    }

    /**
     * Remove carhascolor
     *
     * @param \AppBundle\Entity\CarHasColor $carhascolor
     */
    public function removeCarhascolor(\AppBundle\Entity\CarHasColor $carhascolor)
    {
        $this->carhascolor->removeElement($carhascolor);
    }

    /**
     * Get carhascolor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarhascolor()
    {
        return $this->carhascolor;
    }

    public function __toString()
    {
        return $this->name ? $this->name : "N/D";
    }
}
