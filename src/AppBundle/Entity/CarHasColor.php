<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarHasColor
 *
 * @ORM\Table(name="car_has_color")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarHasColorRepository")
 */
class CarHasColor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Car", inversedBy="carhascolor")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id")
     */
    private $car;
    
    /**
     * @ORM\ManyToOne(targetEntity="Color", inversedBy="carhascolor")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    private $color;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CarHasColor
     */
    public function setCreatedAt($createdAt = null)
    {
        $this->createdAt = new \DateTime('now');

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set car
     *
     * @param \AppBundle\Entity\Car $car
     *
     * @return CarHasColor
     */
    public function setCar(\AppBundle\Entity\Car $car = null)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return \AppBundle\Entity\Car
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return CarHasColor
     */
    public function setColor(\AppBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    public function __toString()
    {
        $col = $this->color ? $this->color->getName(): "-";
        $car = $this->car ? $this->car->getName(): "-";

        return $car." de couleur ".$col;
    }
}
