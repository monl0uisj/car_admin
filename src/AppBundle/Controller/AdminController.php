<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Car;
use AppBundle\Form\CarType;
use AppBundle\Form\ColorType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\CarHasColor;
use AppBundle\Entity\Color;
class AdminController extends Controller
{
    public static $texts = array(
        0 => "Une erreur est survenue",
        1 => "Voiture inconnue",
        2 => "Mise à jour effectuée avec succès",
        3 => "Suppression effectuée avec succès",
        4 => "Couleur inconnue",
        5 => "Suppression annulée! Cet item est utilisé."
    );

    /**
     * @Route("/car-admin/", name="index")
     */
    public function indexAction(Request $request)
    {
        $users = $this->getDoctrine()
        ->getRepository('AppBundle:User')->getUsers();
        
        $voitures = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->getCars();
        
        $couleurs = $this->getDoctrine()
        ->getRepository('AppBundle:Color')->findAll();

        return $this->render('admin/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'users' => $users,
            'voitures' => $voitures,
            'couleurs' => $couleurs
        ]);
    }
    
    /**
     * @Route("/new/car/{car_id}", defaults={"car_id" = 0}, name="carnew")
     */
    public function carnewAction($car_id, Request $request)
    {
        //voiture existante
        $car = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->find($car_id);

        //nouvelle voiture
        if(is_null($car)) {
            $car = new Car();
        }

        //formulaire selon formtype user
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if( strlen( $car->getName() ) < 2 ){
                $form->get("name")->addError(new FormError('Ce nom de voiture est trop court'));
            }

            if ($form->isValid()) {
                //persister entité
                $em = $this->getDoctrine()->getManager();
                $em->persist($car);
                $em->flush();

                //afficher formulaire couleurs
                return new RedirectResponse($this->generateUrl('carcolor', array("car_id"=>$car->getId())));
            }
        }

        //afficher formulaire
        return $this->render('admin/form.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new/color/{color_id}", defaults={"color_id" = 0}, name="colornew")
     */
    public function colornewAction($color_id, Request $request)
    {
        //couleur existante
        $color = $this->getDoctrine()
        ->getRepository('AppBundle:Color')->find($color_id);

        //nouvelle couleur
        if(is_null($color)) {
            $color = new Color();
        }

        //formulaire selon formtype user
        $form = $this->createForm(ColorType::class, $color);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if( strlen( $color->getName() ) < 2 ){
                $form->get("name")->addError(new FormError('Ce nom de couleur est trop court'));
            }

            if ($form->isValid()) {
                //persister entité
                $em = $this->getDoctrine()->getManager();
                $em->persist($color);
                $em->flush();

                //afficher formulaire couleurs
                return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>2)));
            }
        }

        //afficher formulaire
        return $this->render('admin/form.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/dialog/{text_id}", name="dialog")
     */
    public function dialogAction($text_id)
    {
        $text = isset(self::$texts[$text_id]) ? self::$texts[$text_id] : "Erreur inconnue";
        return $this->render('admin/dialog.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'text' => $text
        ]);
    }
    
    /**
     * @Route("/car/color/{car_id}", name="carcolor")
     */
    public function couleurAction($car_id, Request $request)
    {
        $car = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->find($car_id);

        //si la voiture n'existe pas renvoyer vers page erreur
        if(is_null($car)){
            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>1)));
        }

        //liste de toutes les couleurs
        $couleurs = $this->getDoctrine()
        ->getRepository('AppBundle:Color')->findAll();

        //liste des couleurs cochées
        $couleurs_cochees = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->getAvailableColors($car_id);
        
        //liste des couleurs
        $choix = array();
        foreach($couleurs as $couleur){
            $choix[$couleur->getName()] = $couleur->getId();
        }
        
        $form = $this->createFormBuilder(array())
            ->add('colors', ChoiceType::class, array(
                'multiple' => true,
                'expanded' => true,
                'choices'=> $choix,
                //pré-cocher les couleurs déjà choisies
                'choice_attr' => function($id, $key, $index)use($couleurs_cochees) {
                    $checked = array_filter($couleurs_cochees, function($cochee)use($id){
                        return intval($cochee['id']) == $id;
                    });
                    
                    // adds a class like attending_yes, attending_no, etc
                    return ['checked' => count($checked)>0 ];
                }
            ))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $data = $form->getData();
            $color_ids = $data["colors"];
            
            //enlever les associations
            $this->getDoctrine()
            ->getRepository('AppBundle:Car')->removeColors($car_id);
            
            //créer de nouvelles associations
            foreach($color_ids as $id){
                $color = $this->getDoctrine()
                ->getRepository('AppBundle:Color')->find($id);

                $carhascolor = new CarHasColor();
                $carhascolor->setCreatedAt();
                $carhascolor->setCar($car);
                $carhascolor->setColor($color);

                $em = $this->getDoctrine()->getManager();
                $em->persist($carhascolor);
                $em->flush();
            }

            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>2)));
            
        }

        //afficher formulaire
        return $this->render('admin/formcoul.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'cochees' => $couleurs_cochees,
            'couleurs' => $couleurs,
        ]);
    }

    /**
     * @Route("/car/delete/{car_id}", name="cardelete")
     */
    public function deleteAction($car_id, Request $request)
    {
        $car = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->find($car_id);

        //si la voiture n'existe pas renvoyer vers page erreur
        if(is_null($car)){
            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>1)));
        }

        //confirmer via un submit
        $form = $this->createFormBuilder(array())->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $this->getDoctrine()->getRepository('AppBundle:Car')->removeColors($car_id);
            
            $em = $this->getDoctrine()->getManager();
            $em->remove($car);
            $em->flush();
    
            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>3)));
        }

        return $this->render('admin/confirmation.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/color/delete/{color_id}", name="colordelete")
     */
    public function deletecolorAction($color_id, Request $request)
    {
        $color = $this->getDoctrine()
        ->getRepository('AppBundle:Color')->find($color_id);

        //si la couleur n'existe pas renvoyer vers page erreur
        if(is_null($color)){
            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>4)));
        }

        //confirmer via un submit
        $form = $this->createFormBuilder(array())->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            
            $em = $this->getDoctrine()->getManager();
            try{
                $em->remove($color);
                $em->flush();
            }catch(\Exception $e) {
                return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>5)));
            }

            return new RedirectResponse($this->generateUrl('dialog', array("text_id"=>3)));
        }

        return $this->render('admin/confirmation.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView()
        ]);
    }
}
