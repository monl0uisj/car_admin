<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormError;

use AppBundle\Entity\Car;
use AppBundle\Entity\Color;
use AppBundle\Entity\CarHasColor;
use AppBundle\Entity\User;

use AppBundle\Form\UserType;


class FrontController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function accueilAction(Request $request)
    {
        $user = new User();
        //formulaire selon formtype user
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if( strlen( $user->getFirstname() ) < 2 ){
                $form->get("firstname")->addError(new FormError('Ce prénom est trop court'));
            }

            if( strlen( $user->getLastname() ) < 2 ){
                $form->get("lastname")->addError(new FormError('Ce nom est trop court'));
            }

         	if ($form->isValid()) {
                 //manipulation date
                $date_str = $user->getDateOfBirth();
                $date_arr = explode("/", $date_str);
                $new_date = date_create();
                date_date_set($new_date, $date_arr[2], $date_arr[0],$date_arr[1]);
                $user->setDateOfBirth($new_date);

                //persister entité
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                
                //"redirect" vers page suivante ou "render" un merci!
                return $this->render('default/merci.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR
                ]);
            }
        }

        return $this->render('default/form.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/colors/", name="colors")
     */
    public function colorsAction(Request $request)
    {
        $car_id = $request->get('car_id');
        $colors = $this->getDoctrine()
        ->getRepository('AppBundle:Car')->getAvailableColors($car_id);
        $html = "";
        foreach($colors as $color){
             $html .= "<option value='".$color["id"] ."'>".$color["name"] ."</option>";
        }
        return new Response($html);
    }
}