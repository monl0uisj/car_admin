<?php
namespace AppBundle\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CarHasColorAdmin extends AbstractAdmin{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('createdAt', 'datetime');
        $formMapper->add('car');
        $formMapper->add('color');

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('car');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('car');
        $listMapper->add('color');
    }
}