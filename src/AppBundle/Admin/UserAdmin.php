<?php
namespace AppBundle\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('firstname', 'text', array('label'=> 'prénom'));
        $formMapper->add('lastname', 'text', array('label'=> 'nom'));
        $formMapper->add('dateOfBirth', 'date', array('label'=> 'date de naissance', 'years'=>range(1901,2005)));
        $formMapper->add('hasDriverLicence', null, array('label'=> 'Permis de conduire?'));
        $formMapper->add('car');
        $formMapper->add('color');
        
    }
    
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('firstname');
        $datagridMapper->add('lastname');
    }
    
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('firstname');
        $listMapper->add('lastname');
        $listMapper->add('dateOfBirth');
        $listMapper->add('car');
        $listMapper->add('color');
    }
}