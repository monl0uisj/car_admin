caradmin
========

A Symfony project created on November 7, 2017, 7:44 pm.

composer update

php bin/console assets:install

php bin/console doctrine:schema:update --force

php bin/console server:start


accès formulaire: /

accès admin: /car-admin

*optionnel*
accès entités /sonataadmin